import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { Course } from '@prisma/client';
import { CourseService } from './course.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Controller('course')
export class CourseController {
  constructor(private readonly _courseService: CourseService) {}

  @Post('/create')
  async createCourse(@Body() createCourseDto: CreateCourseDto): Promise<Course> {
    return await this._courseService.create(createCourseDto);
  }

  @Get('/all')
  async findAllCourses(): Promise<Course[]> {
    return await this._courseService.findAll();
  }

  @Get('/one/:id')
  async findCourseById(@Param('id') id: string): Promise<Course> {
    return this._courseService.findById(id);
  }
  @Patch('/update/:id')
  async updateCourseById(@Param('id') id: string, @Body() body: UpdateCourseDto): Promise<Course> {
    return await this._courseService.updateById(id, body);
  }
  @Delete('/delete/:id')
  async deleteCourseById(@Param('id') id: string): Promise<Boolean> {
    try {
      const deletedCourse: Course = await this._courseService.deleteById(id);
      if(deletedCourse!==undefined) return true;
      else false;
    }catch(err) {
      console.log(err.meta.cause);
      return false;
    }
  }
}
