import { Course, PrismaClient } from "@prisma/client";
import { CreateCourseDto } from "./dto/create-course.dto";
import { UpdateCourseDto } from "./dto/update-course.dto";

export class CourseRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async create(createCourseDto: CreateCourseDto): Promise<Course>{ console.log(createCourseDto)
        try {
            const res = await this.client.course.create({
                data: createCourseDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async findAll(): Promise<Course[]>{
        try {
            const res: Course[] = await this.client.course.findMany();
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<Course>{
        try {
            const res: Course = await this.client.course.findUnique({where: {id: id}});
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateUserDto: UpdateCourseDto): Promise<Course>{
        try {
            const res = await this.client.course.update({
                where: {id: id},
                data: updateUserDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<Course>{
        try {
            const res: Course = await this.client.course.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }

}