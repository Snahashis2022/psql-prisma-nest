import { Injectable } from '@nestjs/common';
import { Course } from '@prisma/client';
import { CourseRepo } from './course.repo';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CourseService {
  constructor(private readonly _courseRepo: CourseRepo){}

  async create(createCourseDto: CreateCourseDto): Promise<Course> {
    try {
        const course = await this._courseRepo.create(createCourseDto);
        return course;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<Course[]> {
    try {
        const res: Course[] = await this._courseRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<Course>{
    try {
        const res: Course = await this._courseRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateCourseDto: UpdateCourseDto): Promise<Course>{
    try {
        const res = await this._courseRepo.updateById(id, updateCourseDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<Course>{
    try {
        const res = await this._courseRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
}
