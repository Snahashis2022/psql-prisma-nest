import { PartialType } from '@nestjs/mapped-types';
import { CourseEnrollment, Test } from '@prisma/client';
import { IsNotEmpty } from 'class-validator';
import { CreateCourseDto } from './create-course.dto';

export class UpdateCourseDto extends PartialType(CreateCourseDto) {
    @IsNotEmpty()
    courseEnrollment: CourseEnrollment[];

    @IsNotEmpty()
    tests: Test[];
}
