import { Module } from '@nestjs/common';
import { CourseService } from './course.service';
import { CourseController } from './course.controller';
import { CourseRepo } from './course.repo';

@Module({
    imports: [],
    controllers: [CourseController],
    providers: [CourseService, CourseRepo]
})
export class CourseModule {}
