import { IsNotEmpty, IsString, IsUUID } from "class-validator";

export class CreateTestResultDto {
    @IsString()
    @IsNotEmpty()
    result: number;
    
    @IsUUID()
    @IsNotEmpty()
    testId: string;
}
