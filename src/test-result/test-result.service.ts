import { Injectable } from '@nestjs/common';
import { TestResult } from '@prisma/client';
import { CreateTestResultDto } from './dto/create-test-result.dto';
import { UpdateTestResultDto } from './dto/update-test-result.dto';
import { TestResultRepo } from './test-result.repo';

@Injectable()
export class TestResultService {
  constructor(private readonly _testResultRepo: TestResultRepo){}
  async createTestResult(createTestResultDto: CreateTestResultDto): Promise<TestResult> {
    try {
        const testResult: TestResult = await this._testResultRepo.createTestResult(createTestResultDto);
        return testResult;
    }catch(err) {
        throw err; 
    }
  }

  findAll() {
    return `This action returns all testResult`;
  }

  findOne(id: number) {
    return `This action returns a #${id} testResult`;
  }

  update(id: number, updateTestResultDto: UpdateTestResultDto) {
    return `This action updates a #${id} testResult`;
  }

  remove(id: number) {
    return `This action removes a #${id} testResult`;
  }
}
