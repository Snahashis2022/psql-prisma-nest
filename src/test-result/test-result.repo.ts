import { PrismaClient, Test, TestResult } from "@prisma/client";
import { CreateTestResultDto } from "./dto/create-test-result.dto";


export class TestResultRepo{
    client: PrismaClient;

    constructor(){
        this.client = new PrismaClient();
    }
    
    async createTestResult(createTestResultDto: CreateTestResultDto): Promise<TestResult>{
        try {
            const testResult: TestResult = await this.client.testResult.create({
                data: {...createTestResultDto}
            });
            return testResult;
        }catch(err) {
            throw err; 
        }
    }
    

}