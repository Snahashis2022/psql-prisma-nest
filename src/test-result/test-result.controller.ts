import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { TestResultService } from './test-result.service';
import { CreateTestResultDto } from './dto/create-test-result.dto';
import { UpdateTestResultDto } from './dto/update-test-result.dto';

@Controller('test-result')
export class TestResultController {
  constructor(private readonly _testResultService: TestResultService) {}

  @Post()
  create(@Body() createTestResultDto: CreateTestResultDto) {
    return this._testResultService.createTestResult(createTestResultDto);
  }

  @Get()
  findAll() {
    return this._testResultService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this._testResultService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTestResultDto: UpdateTestResultDto) {
    return this._testResultService.update(+id, updateTestResultDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this._testResultService.remove(+id);
  }
}
