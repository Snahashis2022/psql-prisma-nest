import { Module } from '@nestjs/common';
import { TestResultService } from './test-result.service';
import { TestResultController } from './test-result.controller';
import { TestResultRepo } from './test-result.repo';

@Module({
  controllers: [TestResultController],
  providers: [TestResultService, TestResultRepo]
})
export class TestResultModule {}
