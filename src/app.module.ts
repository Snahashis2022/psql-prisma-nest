import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { CourseModule } from './course/course.module';
import { CourseEnrollmentModule } from './course-enrollment/course-enrollment.module';
import { TestModule } from './test/test.module';
import { TestResultModule } from './test-result/test-result.module';
import { HlpModule } from './hlp_modules/hlp.module';

@Module({
  imports: [UserModule, CourseModule, CourseEnrollmentModule, TestModule, TestResultModule, HlpModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
