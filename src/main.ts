import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { HttpException, NotFoundException } from '@nestjs/common';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
  app.use(bodyParser.json()); 
  app.use(bodyParser.urlencoded({extended:true}));
}
bootstrap();
// type a = {user: {name: string}};
// type b = {user: {id: number}};
// type c = a & b;
// let ab:c={user: {name: "string",id:2}};
// interface Inter{
//   meth():void;
// }
// class Test implements Inter{
//   constructor(name: string, age:number){
//     this.name =name;
//     this.age=age;
//   }
  
//   meth():void{
//   throw new NotFoundException("Error")
//   }
//   public name:string;
//   public age:number;
//   private test: string;
//   toString():void{
//     console.log(this.name, this.age)
//   }
//   protected method(marks: number, age: number): number{
//     return marks+age;
//   }

  
// }
// class Test2 extends Test{
//   nothing() {
//     throw new Error('Method not implemented.');
//   }
//   constructor(name: string, age: number, total: number){
//     super(name, age);
//     this.total=total;
//   }
//   public total: number;
//   method2(){
//     this.method(22,65)
//     console.log("im method2")
//   }


// }

// class A {
//   private name: string;
  
// }


// let f=()=>{
//   let a: Test=new Test("sayan", 23);
//   console.log(a.name+a.age);
//   a.toString();
//   let b: Test2=new Test2("sayan",23,24);

//   let c: object ={
//     name: "sayan", age:23
//   }
//   let arr=[{name: "sayan", age:23}, {name: "sayan", age:23}]
//   arr[0].name
//   // console.log(a.method(100,23));
// }

// f();

// //bootstrap();
// // //!Javascript array methods
// let arr: {name: string, age: number}[] = [{name:"snahashis", age:26},{name:"Vivek", age:25},{name:"Kaushik", age: 27}]; 
// let a=arr.filter(x => x.name!=="snahashis");
// let found = arr.find(x => x.name==="snahashis"); //Return the first one that matches
// let newArr=arr.map(x => x.age);
// let some = arr.some(x => x.age>27);
// let every = arr.every(x => x.age>0);
// let lastTwo = arr.slice(-2); //splice does the same but deletes from the original array
// let b=0;
// for(let x of arr){
//   b+=x.age
// }
// let sum:number = arr.reduce((acc,val, index, array) =>{return acc+val.age} ,0);
// let max = arr.reduce((acc,val, index, array) =>{
//   if(acc>val.age){
//     return acc;
//   }else return val;
// } ,-Infinity);
// console.log(a);
// console.log(newArr);
// console.log(b);
// console.log(found);
// console.log(some);
// console.log(every);
// console.log(sum);
// console.log(max);
// console.log(lastTwo);