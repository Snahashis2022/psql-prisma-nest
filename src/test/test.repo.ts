import { Course, PrismaClient, Test } from "@prisma/client";

import { CreateTestDto } from "./dto/create-test.dto";
import { UpdateTestDto } from "./dto/update-test.dto";

export class TestRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    async createTest(createTestDto: CreateTestDto): Promise<Test>{
        try {
            const result: Test = await this.client.test.create({
                data: createTestDto
            });
            return result;
        }catch(err) {
            throw err; 
        }
    }
    async findAll(): Promise<Test[]>{
        try {
            const result: Test[] = await this.client.test.findMany();
            return result; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<Test>{
        try {
            const result: Test = await this.client.test.findUnique({where: {id: id}});
            return result; 
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateTestDto: UpdateTestDto): Promise<Test>{
        try {
            const result: Test = await this.client.test.update({
                where: {id: id},
                data: updateTestDto
            });
            return result; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<Test>{
        try {
            const result: Test = await this.client.test.delete({
                where: {id: id}
            });
            return result; 
        }catch(err) {
            throw err; 
        }
    }
    

}