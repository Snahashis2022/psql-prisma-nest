import { Injectable } from '@nestjs/common';
import { Test } from '@prisma/client';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';
import { TestRepo } from './test.repo';

@Injectable()
export class TestService {
  constructor(private readonly _testRepo: TestRepo){}
  async createTest(createTestDto: CreateTestDto): Promise<Test> {
    try {
        const test = await this._testRepo.createTest(createTestDto);
        return test;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<Test[]>{
    try {
        const tests: Test[] = await this._testRepo.findAll();
        return tests; 
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<Test>{
    try {
        const test: Test = await this._testRepo.findById(id);
        return test; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateTestDto: UpdateTestDto): Promise<Test>{
    try {
        const test = await this._testRepo.updateById(id, updateTestDto);
        return test; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<Test>{
    try {
        const test = await this._testRepo.deleteById(id);
        return test; 
    }catch(err) {
        throw err; 
    }
  }

  
}
