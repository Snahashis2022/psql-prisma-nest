import { Module } from '@nestjs/common';
import { TestService } from './test.service';
import { TestController } from './test.controller';
import { TestRepo } from './test.repo';

@Module({
  controllers: [TestController],
  providers: [TestService, TestRepo]
})
export class TestModule {}
