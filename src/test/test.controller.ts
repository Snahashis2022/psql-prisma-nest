import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { TestService } from './test.service';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';
import { Test } from '@prisma/client';

@Controller('test')
export class TestController {
  constructor(private readonly _testService: TestService) {}

  @Post('/create')
  async createTest(@Body() createTestDto: CreateTestDto): Promise<Test> {
    return await this._testService.createTest(createTestDto);
  }

  @Get('/all')
  async findAll() {
    return await this._testService.findAll();
  }

  @Get('/one/:id')
  async findTestById(@Param('id') id: string) {
    return await this._testService.findById(id);
  }

  @Patch('/update/:id')
  async updateTestById(@Param('id') id: string, @Body() updateTestDto: UpdateTestDto) {
    return await this._testService.updateById(id, updateTestDto);
  }

  @Delete('/delete/:id')
  async deleteTestById(@Param('id') id: string) {
    return await this._testService.deleteById(id);(+id);
  }
}
