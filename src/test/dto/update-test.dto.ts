import { PartialType } from '@nestjs/mapped-types';
import { Test, TestResult } from '@prisma/client';
import { IsNotEmpty, IsString } from 'class-validator';
import { CreateTestDto } from './create-test.dto';

export class UpdateTestDto extends PartialType(CreateTestDto) {
    
    
}
