import { Course } from "@prisma/client";
import { IsNotEmpty, IsString, IsUUID } from "class-validator";

export class CreateTestDto {
    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string;

    @IsString()
    @IsUUID()
    @IsNotEmpty()
    courseId: string;
    
}


