import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CourseEnrollment } from '@prisma/client';
import { CourseEnrollmentService } from './course-enrollment.service';
import { CreateCourseEnrollmentDto } from './dto/create-course-enrollment.dto';
import { UpdateCourseEnrollmentDto } from './dto/update-course-enrollment.dto';

@Controller('course-enrollment')
export class CourseEnrollmentController {
  constructor(private readonly _courseEnrollmentService: CourseEnrollmentService) {}

  @Post('/create')
  async create(@Body() createCourseEnrollmentDto: CreateCourseEnrollmentDto): Promise<CourseEnrollment> {
    return await this._courseEnrollmentService.create(createCourseEnrollmentDto);
  }

  @Get('/all')
  async findAll() {
    return await this._courseEnrollmentService.findAll();
  }

  @Get('/one/:id')
  async findTestById(@Param('id') id: string) {
    return await this._courseEnrollmentService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateCourseEnrollmentDto: UpdateCourseEnrollmentDto) {
    return await this._courseEnrollmentService.updateById(id, updateCourseEnrollmentDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string) {
    return await this._courseEnrollmentService.deleteById(id);
  }
}
