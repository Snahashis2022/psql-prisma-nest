import { CourseEnrollment, PrismaClient, User } from "@prisma/client";
import { CreateCourseEnrollmentDto } from "./dto/create-course-enrollment.dto";
import { UpdateCourseEnrollmentDto } from "./dto/update-course-enrollment.dto";

export class CourseEnrollmentRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    async create(user: CreateCourseEnrollmentDto): Promise<CourseEnrollment>{
        try {
            const savedUser: CourseEnrollment = await this.client.courseEnrollment.create({
                data: user,
            });
            return savedUser;
        }catch(err) {
            throw err; 
        }
    }
    async findAll(): Promise<CourseEnrollment[]>{
        try {
            const res: CourseEnrollment[] = await this.client.courseEnrollment.findMany();
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<CourseEnrollment>{
        try {
            const res: CourseEnrollment = await this.client.courseEnrollment.findUnique({where: {id: id}});
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateCourseEnrollmentDto: UpdateCourseEnrollmentDto): Promise<CourseEnrollment>{
        try {
            const res = await this.client.courseEnrollment.update({
                where: {id: id},
                data: updateCourseEnrollmentDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<CourseEnrollment>{
        try {
            const res: CourseEnrollment = await this.client.courseEnrollment.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }

    
}