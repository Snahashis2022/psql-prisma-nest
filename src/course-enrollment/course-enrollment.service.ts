import { Injectable } from '@nestjs/common';
import { CourseEnrollment } from '@prisma/client';
import { CourseEnrollmentRepo } from './course-enrollment.repo';
import { CreateCourseEnrollmentDto } from './dto/create-course-enrollment.dto';
import { UpdateCourseEnrollmentDto } from './dto/update-course-enrollment.dto';

@Injectable()
export class CourseEnrollmentService {
  constructor(private readonly _courseEnrollmentRepo: CourseEnrollmentRepo){}
  async create(createCourseEnrollmentDto: CreateCourseEnrollmentDto): Promise<CourseEnrollment> {
    try {
        const res = await this._courseEnrollmentRepo.create(createCourseEnrollmentDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<CourseEnrollment[]>{
    try {
        const res: CourseEnrollment[] = await this._courseEnrollmentRepo.findAll();
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<CourseEnrollment>{
    try {
        const res: CourseEnrollment = await this._courseEnrollmentRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateCourseEnrollmentDto: UpdateCourseEnrollmentDto): Promise<CourseEnrollment>{
    try {
        const res = await this._courseEnrollmentRepo.updateById(id, updateCourseEnrollmentDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<CourseEnrollment>{
    try {
        const res = await this._courseEnrollmentRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
}
