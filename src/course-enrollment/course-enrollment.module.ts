import { Module } from '@nestjs/common';
import { CourseEnrollmentService } from './course-enrollment.service';
import { CourseEnrollmentController } from './course-enrollment.controller';
import { CourseEnrollmentRepo } from './course-enrollment.repo';

@Module({
    imports: [],
    controllers: [CourseEnrollmentController],
    providers: [CourseEnrollmentService, CourseEnrollmentRepo]
})

export class CourseEnrollmentModule {}
