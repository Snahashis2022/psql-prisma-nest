import { IsUUID, IsString, IsNotEmpty } from "class-validator";

export enum UserRole{
    TEACHER = "TEACHER",
    STUDENT = "STUDENT"
}
export class CreateCourseEnrollmentDto {
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    userId: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    courseId: string;
    
    @IsNotEmpty()
    role: UserRole
}
