import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from '@prisma/client';
import e from 'express';

@Controller('user')
export class UserController {
  constructor(private readonly _userService: UserService) {}

  @Post('/create')
  async createUser(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this._userService.create(createUserDto);
  }

  @Get('/all')
  async findAllUsers(): Promise<User[]> {
    return await this._userService.findAll();
  }

  @Get('/one/:id')
  async findUserById(@Param('id') id: string): Promise<User> {
    return this._userService.findById(id);
  }
  @Patch('/update/:id')
  async updateUserById(@Param('id') id: string, @Body() body: UpdateUserDto): Promise<User> {
    return await this._userService.updateById(id, body);
  }
  @Delete('/delete/:id')
  async deleteUserById(@Param('id') id: string): Promise<Boolean> {
    try {
      const deletedUser: User = await this._userService.deleteById(id);
      if(deletedUser!==undefined) return true;
      else false;
    }catch(err) {
      console.log(err.meta.cause);
      return false;
    }
  }


}
