import { Injectable } from '@nestjs/common';
import { User } from '@prisma/client';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRepo } from './user.repo';

@Injectable()
export class UserService {
  constructor(private readonly _userRepo: UserRepo){

  }
  async create(createUserDto: CreateUserDto): Promise<User> {
    try {
        const user: User = await this._userRepo.create(createUserDto);
        return user;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<User[]> {
    try {
        const users: User[] = await this._userRepo.findAll();
        return users;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<User>{
    try {
        const user: User = await this._userRepo.findById(id);
        return user; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateUserDto: UpdateUserDto): Promise<User>{
    try {
        const user = await this._userRepo.updateById(id, updateUserDto);
        return user; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<User>{
    try {
        const user = await this._userRepo.deleteById(id);
        return user; 
    }catch(err) {
        throw err; 
    }
  }
  async updateMany(id: string, updateUserDto: UpdateUserDto){
    try {
        const user = await this._userRepo.updateMany(id, updateUserDto);
        return user; 
    }catch(err) {
        throw err; 
    }
  }
}
