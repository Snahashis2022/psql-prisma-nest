export class UserEntity {
    name: string;
    email: string;
    password: string;
    gender: string;
    age: number;
    courseEnrollment: any;
}
