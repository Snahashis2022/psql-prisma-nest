import { PartialType } from '@nestjs/mapped-types';
import { CourseEnrollment } from '@prisma/client';
import { IsNotEmpty } from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
    @IsNotEmpty()
    courseEnrollment: CourseEnrollment[];
}
