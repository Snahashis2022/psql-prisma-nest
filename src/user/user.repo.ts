import { PrismaClient, User } from "@prisma/client";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import { UserEntity } from "./entities/user.entity";
export class UserRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    async create(user: CreateUserDto): Promise<User>{
        try {
            const savedUser: User = await this.client.user.create({
                data: user,
            });
            return savedUser;
        }catch(err) {
            throw err; 
        }
    }
    async findAll(): Promise<User[]>{
        try {
            const users: User[] = await this.client.user.findMany();
            return users; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<User>{
        try {
            const users: User = await this.client.user.findUnique({where: {id: id}});
            return users; 
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateUserDto: UpdateUserDto): Promise<User>{
        try {
            const users = await this.client.user.update({
                where: {id: id},
                data: updateUserDto
            });
            return users; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<User>{
        try {
            const users: User = await this.client.user.delete({
                where: {id: id}
            });
            return users; 
        }catch(err) {
            throw err; 
        }
    }
    async updateMany(id: string, updateUserDto: UpdateUserDto){
        try {
            const users = await this.client.user.updateMany({
                where: {
                    age: 23
                },
                data: {...updateUserDto}
            });
            return users; 
        }catch(err) {
            throw err; 
        }
    }
    
    
}