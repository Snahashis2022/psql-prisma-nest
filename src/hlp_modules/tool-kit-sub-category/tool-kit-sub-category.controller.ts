import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ToolKitSubCategoryService } from './tool-kit-sub-category.service';
import { CreateToolKitSubCategoryDto } from './dto/create-tool-kit-sub-category.dto';
import { UpdateToolKitSubCategoryDto } from './dto/update-tool-kit-sub-category.dto';
import { Tool_kit_sub_category as ToolKitSubCategory} from "@prisma/client";

@Controller('tool-kit-sub-category')
export class ToolKitSubCategoryController {
  constructor(private readonly _toolKitSubCategoryService: ToolKitSubCategoryService) {}

  @Post('/create')
  async create(@Body() createToolKitSubCategoryDto: CreateToolKitSubCategoryDto): Promise<ToolKitSubCategory> {
    return await this._toolKitSubCategoryService.create(createToolKitSubCategoryDto);
  }

  @Get('/all')
  async findAll(): Promise<ToolKitSubCategory[]> {
    return await this._toolKitSubCategoryService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<ToolKitSubCategory> {
    return await this._toolKitSubCategoryService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateToolKitSubCategoryDto: UpdateToolKitSubCategoryDto): Promise<ToolKitSubCategory> {
    return await this._toolKitSubCategoryService.updateById(id, updateToolKitSubCategoryDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<ToolKitSubCategory> {
    return await this._toolKitSubCategoryService.deleteById(id);
  }
}
