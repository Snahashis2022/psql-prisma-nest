import { IsNotEmpty, IsString, IsUUID } from "class-validator";

export class CreateToolKitSubCategoryDto {
    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string;
    //Relations
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    tool_kit_category_id: string

}
