import { PartialType } from '@nestjs/mapped-types';
import { CreateToolKitSubCategoryDto } from './create-tool-kit-sub-category.dto';

export class UpdateToolKitSubCategoryDto extends PartialType(CreateToolKitSubCategoryDto) {}
