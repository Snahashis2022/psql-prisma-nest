import { Injectable } from '@nestjs/common';
import { CreateToolKitSubCategoryDto } from './dto/create-tool-kit-sub-category.dto';
import { UpdateToolKitSubCategoryDto } from './dto/update-tool-kit-sub-category.dto';
import { Tool_kit_sub_category as ToolKitSubCategory} from "@prisma/client";
import { ToolKitSubCategoryRepo } from './tool-kit-sub-category.repo';
@Injectable()
export class ToolKitSubCategoryService {
  constructor(private readonly _toolKitSubCategoryRepo: ToolKitSubCategoryRepo){}

  async findAll(): Promise<ToolKitSubCategory[]> {
    try {
        const res: ToolKitSubCategory[] = await this._toolKitSubCategoryRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<ToolKitSubCategory>{
    try {
        const res: ToolKitSubCategory = await this._toolKitSubCategoryRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<ToolKitSubCategory>{
    try {
        const res = await this._toolKitSubCategoryRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateToolKitSubCategoryDto: UpdateToolKitSubCategoryDto): Promise<ToolKitSubCategory>{
    try {
        const res = await this._toolKitSubCategoryRepo.updateById(id, updateToolKitSubCategoryDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createToolKitSubCategoryDto: CreateToolKitSubCategoryDto): Promise<ToolKitSubCategory> {
    try {
        const res = await this._toolKitSubCategoryRepo.create(createToolKitSubCategoryDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
