import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitSubCategoryController } from '../tool-kit-sub-category.controller';
import { ToolKitSubCategoryService } from '../tool-kit-sub-category.service';

describe('ToolKitSubCategoryController', () => {
  let controller: ToolKitSubCategoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ToolKitSubCategoryController],
      providers: [ToolKitSubCategoryService],
    }).compile();

    controller = module.get<ToolKitSubCategoryController>(ToolKitSubCategoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
