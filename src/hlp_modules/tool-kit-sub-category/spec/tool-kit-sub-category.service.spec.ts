import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitSubCategoryService } from '../tool-kit-sub-category.service';

describe('ToolKitSubCategoryService', () => {
  let service: ToolKitSubCategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ToolKitSubCategoryService],
    }).compile();

    service = module.get<ToolKitSubCategoryService>(ToolKitSubCategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
