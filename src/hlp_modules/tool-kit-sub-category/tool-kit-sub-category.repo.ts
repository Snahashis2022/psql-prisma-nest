import { PrismaClient, Tool_kit_sub_category as ToolKitSubCategory} from "@prisma/client";
import { CreateToolKitSubCategoryDto } from "./dto/create-tool-kit-sub-category.dto";
import { UpdateToolKitSubCategoryDto } from "./dto/update-tool-kit-sub-category.dto";






export class ToolKitSubCategoryRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<ToolKitSubCategory[]>{
        try {
            const res: ToolKitSubCategory[] = await this.client.tool_kit_sub_category.findMany({
                include:{
                    tool_kit_category: true,
                    tool_kits: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<ToolKitSubCategory>{
        try {
            const res: ToolKitSubCategory = await this.client.tool_kit_sub_category.findUnique({
                where: {id: id},
                include:{
                    tool_kit_category: true,
                    tool_kits: true
                }});
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<ToolKitSubCategory>{
        try {
            const res: ToolKitSubCategory = await this.client.tool_kit_sub_category.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createToolKitSubCategoryDto: CreateToolKitSubCategoryDto): Promise<ToolKitSubCategory>{
        try {
            const res = await this.client.tool_kit_sub_category.create({
                data: createToolKitSubCategoryDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateToolKitSubCategoryDto: UpdateToolKitSubCategoryDto): Promise<ToolKitSubCategory>{
        try {
            const res = await this.client.tool_kit_sub_category.update({
                where: {id: id},
                data: updateToolKitSubCategoryDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}