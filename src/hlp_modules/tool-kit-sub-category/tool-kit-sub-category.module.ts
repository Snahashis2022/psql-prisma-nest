import { Module } from '@nestjs/common';
import { ToolKitSubCategoryService } from './tool-kit-sub-category.service';
import { ToolKitSubCategoryController } from './tool-kit-sub-category.controller';
import { ToolKitSubCategoryRepo } from './tool-kit-sub-category.repo';

@Module({
  controllers: [ToolKitSubCategoryController],
  providers: [ToolKitSubCategoryService, ToolKitSubCategoryRepo]
})
export class ToolKitSubCategoryModule {}
