import { Module } from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import { ScheduleController } from './schedule.controller';
import { ScheduleRepo } from './schedule.repo';

@Module({
  controllers: [ScheduleController],
  providers: [ScheduleService, ScheduleRepo]
})
export class ScheduleModule {}
