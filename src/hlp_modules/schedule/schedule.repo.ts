import { Schedule, PrismaClient } from "@prisma/client";
import { CreateScheduleDto } from "./dto/create-schedule.dto";
import { UpdateScheduleDto } from "./dto/update-schedule.dto";


export class ScheduleRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<Schedule[]>{
        try {
            const res: Schedule[] = await this.client.schedule.findMany({
                include:{
                    check_in: true,
                    tool_kit: true,
                    schedule_reminders: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<Schedule>{
        try {
            const res: Schedule = await this.client.schedule.findUnique({
                where: {id: id},
                include:{
                    check_in: true,
                    tool_kit: true,
                    schedule_reminders: true
                }});
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<Schedule>{
        try {
            const res: Schedule = await this.client.schedule.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createScheduleDto: CreateScheduleDto): Promise<Schedule>{
        try {
            const res = await this.client.schedule.create({
                data: createScheduleDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateSchedulesDto: UpdateScheduleDto): Promise<Schedule>{
        try {
            const res = await this.client.schedule.update({
                where: {id: id},
                data: updateSchedulesDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}