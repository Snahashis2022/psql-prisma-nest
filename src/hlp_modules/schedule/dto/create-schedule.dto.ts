import { User, Check_in, Tool_kit, Schedule_reminder } from "@prisma/client";
import { IsNotEmpty, IsString, IsUUID } from "class-validator";

export enum ScheduleFor {
    TOOL_KIT = "TOOL_KIT",
    CHECK_IN = "CHECK_IN"
}
export enum ScheduleType {
    ONE_TIME = "ONE_TIME",
    DAILY = "DAILY",
    WEEKLY = "WEEKLY",
    MONTHLY = "MONTHLY",
}
export class CreateScheduleDto {
    @IsNotEmpty()
    schedule_for: ScheduleFor;

    @IsNotEmpty()
    schedule_type: ScheduleType;

    @IsString()
    @IsNotEmpty()
    start_date: string;

    @IsNotEmpty()
    show_reminder: boolean;

    @IsNotEmpty()
    repeat_per_day: number;

    @IsNotEmpty()
    repeat_per_month: number[];

    @IsString()
    @IsNotEmpty()
    schedule_days: string[];

    @IsNotEmpty()
    is_schedule_disabled: boolean;
    
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    check_in_id: string | null;
    
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    tool_kit_id: string | null;
}

