import { PartialType } from '@nestjs/mapped-types';
import { Schedule_reminder, User, Check_in, Tool_kit } from '@prisma/client';
import { IsString, IsNotEmpty } from 'class-validator';
import { CreateScheduleDto } from './create-schedule.dto';

export class UpdateScheduleDto extends PartialType(CreateScheduleDto) {
    
}


