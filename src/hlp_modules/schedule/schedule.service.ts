import { Injectable } from '@nestjs/common';
import { Schedule } from '@prisma/client';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import { ScheduleRepo } from './schedule.repo';

@Injectable()
export class ScheduleService {
  constructor(private readonly _scheduleRepo: ScheduleRepo){}
  async findAll(): Promise<Schedule[]> {
    try {
        const res: Schedule[] = await this._scheduleRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<Schedule>{
    try {
        const res: Schedule = await this._scheduleRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<Schedule>{
    try {
        const res = await this._scheduleRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateScheduleDto: UpdateScheduleDto): Promise<Schedule>{
    try {
        const res = await this._scheduleRepo.updateById(id, updateScheduleDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createScheduleDto: CreateScheduleDto): Promise<Schedule> {
    try {
        const res = await this._scheduleRepo.create(createScheduleDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
