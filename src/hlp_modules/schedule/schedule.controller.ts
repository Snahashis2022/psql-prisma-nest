import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import { ScheduleRepo } from './schedule.repo';
import { Schedule } from "@prisma/client";

@Controller('schedule')
export class ScheduleController {
  constructor(private readonly _scheduleService: ScheduleService) { }

  @Post('/create')
  async create(@Body() createScheduleDto: CreateScheduleDto): Promise<Schedule> {
    return await this._scheduleService.create(createScheduleDto);
  }

  @Get('/all')
  async findAll(): Promise<Schedule[]> {
    return await this._scheduleService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<Schedule> {
    return await this._scheduleService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateScheduleDto: UpdateScheduleDto): Promise<Schedule> {
    return await this._scheduleService.updateById(id, updateScheduleDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<Schedule> {
    return await this._scheduleService.deleteById(id);
  }
}
