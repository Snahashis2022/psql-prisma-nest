import { Check_in, ScheduleFor, ScheduleType, Tool_kit } from "@prisma/client";
import { IsNotEmpty, IsString, IsUUID } from "class-validator";
import { ScheduleReminder } from "src/hlp_modules/schedule-reminder/entities/schedule-reminder.entity";


export class ScheduleEntity {
    @IsUUID()
    id: string;

    @IsNotEmpty()
    schedule_for: ScheduleFor;

    @IsNotEmpty()
    schedule_type: ScheduleType;

    @IsString()
    @IsNotEmpty()
    start_date: string;

    @IsNotEmpty()
    show_reminder: boolean;

    @IsNotEmpty()
    repeat_per_day: number;

    @IsNotEmpty()
    repeat_per_month: number[];

    @IsString()
    @IsNotEmpty()
    schedule_days: string[];

    @IsNotEmpty()
    is_schedule_disabled: boolean;
    
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    check_in_id: string | null;
    
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    tool_kit_id: string | null;

    check_in: Check_in;

    tool_kit: Tool_kit;

    schedule_reminders: ScheduleReminder[];
}
