import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitCategoryController } from '../tool-kit-category.controller';
import { ToolKitCategoryService } from '../tool-kit-category.service';

describe('ToolKitCategoryController', () => {
  let controller: ToolKitCategoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ToolKitCategoryController],
      providers: [ToolKitCategoryService],
    }).compile();

    controller = module.get<ToolKitCategoryController>(ToolKitCategoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
