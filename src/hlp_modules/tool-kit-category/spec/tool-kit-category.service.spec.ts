import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitCategoryService } from '../tool-kit-category.service';

describe('ToolKitCategoryService', () => {
  let service: ToolKitCategoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ToolKitCategoryService],
    }).compile();

    service = module.get<ToolKitCategoryService>(ToolKitCategoryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
