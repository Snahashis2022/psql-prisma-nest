import { Injectable } from '@nestjs/common';
import { CreateToolKitCategoryDto } from './dto/create-tool-kit-category.dto';
import { UpdateToolKitCategoryDto } from './dto/update-tool-kit-category.dto';
import { Tool_kit_category as ToolKitCategory} from "@prisma/client";
import { ToolKitCategoryRepo } from './tool-kit-category.repo';

@Injectable()
export class ToolKitCategoryService {
  constructor(private readonly _toolKitCategoryRepo: ToolKitCategoryRepo){}

  async findAll(): Promise<ToolKitCategory[]> {
    try {
        const res: ToolKitCategory[] = await this._toolKitCategoryRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<ToolKitCategory>{
    try {
        const res: ToolKitCategory = await this._toolKitCategoryRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<ToolKitCategory>{
    try {
        const res = await this._toolKitCategoryRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateToolKitCategoryDto: UpdateToolKitCategoryDto): Promise<ToolKitCategory>{
    try {
        const res = await this._toolKitCategoryRepo.updateById(id, updateToolKitCategoryDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createToolKitCategoryDto: CreateToolKitCategoryDto): Promise<ToolKitCategory> {
    try {
        const res = await this._toolKitCategoryRepo.create(createToolKitCategoryDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
