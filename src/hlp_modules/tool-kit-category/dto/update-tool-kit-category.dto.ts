import { PartialType } from '@nestjs/mapped-types';
import { CreateToolKitCategoryDto } from './create-tool-kit-category.dto';

export class UpdateToolKitCategoryDto extends PartialType(CreateToolKitCategoryDto) {}
