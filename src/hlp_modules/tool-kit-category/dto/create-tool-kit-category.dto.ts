import { AgeGroup } from "@prisma/client";
import { IsString, IsNotEmpty, IsUUID } from "class-validator";

export class CreateToolKitCategoryDto {
    @IsString()
    @IsNotEmpty()
    title: string;

    @IsString()
    @IsNotEmpty()
    description: string

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    age_group: AgeGroup;
}
