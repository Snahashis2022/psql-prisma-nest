import { Module } from '@nestjs/common';
import { ToolKitCategoryService } from './tool-kit-category.service';
import { ToolKitCategoryController } from './tool-kit-category.controller';
import { ToolKitCategoryRepo } from './tool-kit-category.repo';

@Module({
  controllers: [ToolKitCategoryController],
  providers: [ToolKitCategoryService, ToolKitCategoryRepo]
})
export class ToolKitCategoryModule {}
