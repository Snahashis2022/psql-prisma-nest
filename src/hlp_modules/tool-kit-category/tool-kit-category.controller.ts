import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ToolKitCategoryService } from './tool-kit-category.service';
import { CreateToolKitCategoryDto } from './dto/create-tool-kit-category.dto';
import { UpdateToolKitCategoryDto } from './dto/update-tool-kit-category.dto';
import { Tool_kit_category as ToolKitCategory} from "@prisma/client";

@Controller('tool-kit-category')
export class ToolKitCategoryController {
  constructor(private readonly _toolKitCategoryService: ToolKitCategoryService) {}

  @Post('/create')
  async create(@Body() createToolKitCategoryDto: CreateToolKitCategoryDto): Promise<ToolKitCategory> {
    return await this._toolKitCategoryService.create(createToolKitCategoryDto);
  }

  @Get('/all')
  async findAll(): Promise<ToolKitCategory[]> {
    return await this._toolKitCategoryService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<ToolKitCategory> {
    return await this._toolKitCategoryService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateToolKitCategoryDto: UpdateToolKitCategoryDto): Promise<ToolKitCategory> {
    return await this._toolKitCategoryService.updateById(id, updateToolKitCategoryDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<ToolKitCategory> {
    return await this._toolKitCategoryService.deleteById(id);
  }
}
