import { PrismaClient, Tool_kit_category as ToolKitCategory} from "@prisma/client";
import { CreateToolKitCategoryDto } from "./dto/create-tool-kit-category.dto";
import { UpdateToolKitCategoryDto } from "./dto/update-tool-kit-category.dto";





export class ToolKitCategoryRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<ToolKitCategory[]>{
        try {
            const res: ToolKitCategory[] = await this.client.tool_kit_category.findMany({
                include:{
                    tool_kits: true,
                    tool_kit_sub_categories: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<ToolKitCategory>{
        try {
            const res: ToolKitCategory = await this.client.tool_kit_category.findUnique({
                where: {id: id},
                include:{
                    tool_kits: true,
                    tool_kit_sub_categories: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<ToolKitCategory>{
        try {
            const res: ToolKitCategory = await this.client.tool_kit_category.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createToolKitCategoryDto: CreateToolKitCategoryDto): Promise<ToolKitCategory>{
        try {
            const res = await this.client.tool_kit_category.create({
                data: createToolKitCategoryDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateToolKitCategoryDto: UpdateToolKitCategoryDto): Promise<ToolKitCategory>{
        try {
            const res = await this.client.tool_kit_category.update({
                where: {id: id},
                data: updateToolKitCategoryDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}