import { Test, TestingModule } from '@nestjs/testing';
import { MembershipStageService } from '../membership-stage.service';

describe('MembershipStageService', () => {
  let service: MembershipStageService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MembershipStageService],
    }).compile();

    service = module.get<MembershipStageService>(MembershipStageService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
