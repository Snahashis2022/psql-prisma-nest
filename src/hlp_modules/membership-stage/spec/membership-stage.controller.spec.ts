import { Test, TestingModule } from '@nestjs/testing';
import { MembershipStageController } from '../membership-stage.controller';
import { MembershipStageService } from '../membership-stage.service';

describe('MembershipStageController', () => {
  let controller: MembershipStageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MembershipStageController],
      providers: [MembershipStageService],
    }).compile();

    controller = module.get<MembershipStageController>(MembershipStageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
