import { Schedule, PrismaClient, Membership_stage as MembershipStage} from "@prisma/client";
import { CreateMembershipStageDto } from "./dto/create-membership-stage.dto";
import { UpdateMembershipStageDto } from "./dto/update-membership-stage.dto";






export class MembershipStageRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<MembershipStage[]>{
        try {
            const res: MembershipStage[] = await this.client.membership_stage.findMany({
                include:{
                    membership_level: true,
                    tool_kits: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<MembershipStage>{
        try {
            const res: MembershipStage = await this.client.membership_stage.findUnique({
                where: {id: id},
                include:{
                    membership_level: true,
                    tool_kits: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<MembershipStage>{
        try {
            const res: MembershipStage = await this.client.membership_stage.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createMembershipStageDto: CreateMembershipStageDto): Promise<MembershipStage>{
        try {
            const res = await this.client.membership_stage.create({
                data: createMembershipStageDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateMembershipStageDto: UpdateMembershipStageDto): Promise<MembershipStage>{
        try {
            const res = await this.client.membership_stage.update({
                where: {id: id},
                data: updateMembershipStageDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}