import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MembershipStageService } from './membership-stage.service';
import { CreateMembershipStageDto } from './dto/create-membership-stage.dto';
import { UpdateMembershipStageDto } from './dto/update-membership-stage.dto';
import { Schedule, PrismaClient, Membership_stage as MembershipStage} from "@prisma/client";

@Controller('membership-stage')
export class MembershipStageController {
  constructor(private readonly _membershipStageService: MembershipStageService) {}

  @Post('/create')
  async create(@Body() createMembershipStageDto: CreateMembershipStageDto): Promise<MembershipStage> {
    return await this._membershipStageService.create(createMembershipStageDto);
  }

  @Get('/all')
  async findAll(): Promise<MembershipStage[]> {
    return await this._membershipStageService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<MembershipStage> {
    return await this._membershipStageService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateMembershipStageDto: UpdateMembershipStageDto): Promise<MembershipStage> {
    return await this._membershipStageService.updateById(id, updateMembershipStageDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<MembershipStage>  {
    return await this._membershipStageService.deleteById(id);
  }

}
