import { IsString, IsNotEmpty, IsUUID } from "class-validator";

export class CreateMembershipStageDto {
    @IsString()
    @IsNotEmpty()
    title: string

    @IsString()
    @IsNotEmpty()
    description: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    membership_level_id: string;
}
