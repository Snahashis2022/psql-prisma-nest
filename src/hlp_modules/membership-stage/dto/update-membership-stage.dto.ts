import { PartialType } from '@nestjs/mapped-types';
import { CreateMembershipStageDto } from './create-membership-stage.dto';

export class UpdateMembershipStageDto extends PartialType(CreateMembershipStageDto) {}
