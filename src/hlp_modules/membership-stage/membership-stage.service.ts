import { Injectable } from '@nestjs/common';
import { CreateMembershipStageDto } from './dto/create-membership-stage.dto';
import { UpdateMembershipStageDto } from './dto/update-membership-stage.dto';
import { Membership_stage as MembershipStage} from "@prisma/client";
import { MembershipStageRepo } from './membership-stage.repo';

@Injectable()
export class MembershipStageService {
  constructor(private readonly _membershipStageRepo: MembershipStageRepo){}

  async findAll(): Promise<MembershipStage[]> {
    try {
        const res: MembershipStage[] = await this._membershipStageRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<MembershipStage>{
    try {
        const res: MembershipStage = await this._membershipStageRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<MembershipStage>{
    try {
        const res = await this._membershipStageRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateMembershipStageDto: UpdateMembershipStageDto): Promise<MembershipStage>{
    try {
        const res = await this._membershipStageRepo.updateById(id, updateMembershipStageDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createMembershipStageDto: CreateMembershipStageDto): Promise<MembershipStage> {
    try {
        const res = await this._membershipStageRepo.create(createMembershipStageDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
