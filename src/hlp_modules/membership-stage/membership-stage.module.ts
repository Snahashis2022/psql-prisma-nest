import { Module } from '@nestjs/common';
import { MembershipStageService } from './membership-stage.service';
import { MembershipStageController } from './membership-stage.controller';
import { MembershipStageRepo } from './membership-stage.repo';

@Module({
  controllers: [MembershipStageController],
  providers: [MembershipStageService, MembershipStageRepo]
})
export class MembershipStageModule {}
