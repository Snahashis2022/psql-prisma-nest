import { Injectable } from '@nestjs/common';
import { Habit } from '@prisma/client';
import { CreateHabitDto } from './dto/create-habit.dto';
import { UpdateHabitDto } from './dto/update-habit.dto';
import { HabitRepo } from './habit.repo';

@Injectable()
export class HabitService {
  constructor(private readonly _habitRepo: HabitRepo ){}
  
  async create(createHabitDto: CreateHabitDto): Promise<Habit> {
    try {
        const res = await this._habitRepo.create(createHabitDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<Habit[]> {
    try {
        const res: Habit[] = await this._habitRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<Habit>{
    try {
        const res: Habit = await this._habitRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateHabitDto: UpdateHabitDto): Promise<Habit>{
    try {
        const res = await this._habitRepo.updateById(id, updateHabitDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }

  async deleteById(id: string): Promise<Habit>{
    try {
        const res = await this._habitRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
}

