import { Module } from '@nestjs/common';
import { HabitService } from './habit.service';
import { HabitController } from './habit.controller';
import { HabitRepo } from './habit.repo';

@Module({
  controllers: [HabitController],
  providers: [HabitService, HabitRepo]
})
export class HabitModule {}
