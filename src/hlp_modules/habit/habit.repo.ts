import { PrismaClient, Habit } from "@prisma/client";
import { CreateHabitDto } from "./dto/create-habit.dto";
import { UpdateHabitDto } from "./dto/update-habit.dto";



export class HabitRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<Habit[]>{
        try {
            const res: Habit[] = await this.client.habit.findMany({
                include:{
                    tool_kits: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<Habit>{
        try {
            const res: Habit = await this.client.habit.findUnique({
                where: {id: id},
                include:{
                    tool_kits: true
                }});
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<Habit>{
        try {
            const res: Habit = await this.client.habit.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createHabitDto: CreateHabitDto): Promise<Habit>{
        try {
            const res = await this.client.habit.create({
                data: createHabitDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateHabitDto: UpdateHabitDto): Promise<Habit>{
        try {
            const res = await this.client.habit.update({
                where: {id: id},
                data: updateHabitDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}