import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { HabitService } from './habit.service';
import { CreateHabitDto } from './dto/create-habit.dto';
import { UpdateHabitDto } from './dto/update-habit.dto';
import { Habit } from "@prisma/client";

@Controller('habit')
export class HabitController {
  constructor(private readonly _habitService: HabitService) {}

  @Post('/create')
  async create(@Body() createHabitDto: CreateHabitDto): Promise<Habit> {
    return await this._habitService.create(createHabitDto);
  }

  @Get('/all')
  async findAll(): Promise<Habit[]> {
    return await this._habitService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<Habit> {
    return await this._habitService.findById(id);
  }

  @Patch('/update/:id')
  async updateTestById(@Param('id') id: string, @Body() updateHabitDto: UpdateHabitDto): Promise<Habit> {
    return await this._habitService.updateById(id, updateHabitDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<Habit>  {
    return await this._habitService.deleteById(id);
  }
}
