import { Module } from '@nestjs/common';
import { CheckInService } from './check-in.service';
import { CheckInController } from './check-in.controller';
import { CheckInRepo } from './check-in.repo';

@Module({
  controllers: [CheckInController],
  providers: [CheckInService, CheckInRepo]
})
export class CheckInModule {}
