import { Injectable } from '@nestjs/common';
import { CreateCheckInDto } from './dto/create-check-in.dto';
import { UpdateCheckInDto } from './dto/update-check-in.dto';
import { Check_in as CheckIn } from "@prisma/client";
import { CheckInRepo } from './check-in.repo';

@Injectable()
export class CheckInService {
  constructor(private readonly _checkInRepo: CheckInRepo ){}
  
  async create(createCheckInDto: CreateCheckInDto): Promise<CheckIn> {
    try {
        const res = await this._checkInRepo.create(createCheckInDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

  async findAll(): Promise<CheckIn[]> {
    try {
        const res: CheckIn[] = await this._checkInRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<CheckIn>{
    try {
        const res: CheckIn = await this._checkInRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateCheckInDto: UpdateCheckInDto): Promise<CheckIn>{
    try {
        const res = await this._checkInRepo.updateById(id, updateCheckInDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }

  async deleteById(id: string): Promise<CheckIn>{
    try {
        const res = await this._checkInRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }

}
