import { PrismaClient, Check_in as CheckIn } from "@prisma/client";
import { CreateCheckInDto } from "./dto/create-check-in.dto";
import { UpdateCheckInDto } from "./dto/update-check-in.dto";



export class CheckInRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<CheckIn[]>{
        try {
            const res: CheckIn[] = await this.client.check_in.findMany({
                include:{
                    schedules: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<CheckIn>{
        try {
            const res: CheckIn = await this.client.check_in.findUnique({
                where: {id: id},
                include:{
                    schedules: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<CheckIn>{
        try {
            const res: CheckIn = await this.client.check_in.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createCheckInDto: CreateCheckInDto): Promise<CheckIn>{
        try {
            const res = await this.client.check_in.create({
                data: createCheckInDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateCheckInDto: UpdateCheckInDto): Promise<CheckIn>{
        try {
            const res = await this.client.check_in.update({
                where: {id: id},
                data: updateCheckInDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}