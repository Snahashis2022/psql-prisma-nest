import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { CheckInService } from './check-in.service';
import { CreateCheckInDto } from './dto/create-check-in.dto';
import { UpdateCheckInDto } from './dto/update-check-in.dto';
import { Check_in as CheckIn } from "@prisma/client";

@Controller('check-in')
export class CheckInController {
  constructor(private readonly _checkInService: CheckInService) {}

  @Post('/create')
  async create(@Body() createTCheckInDto: CreateCheckInDto): Promise<CheckIn> {
    return await this._checkInService.create(createTCheckInDto);
  }

  @Get('/all')
  async findAll(): Promise<CheckIn[]> {
    return await this._checkInService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<CheckIn> {
    return await this._checkInService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateCheckInDto: UpdateCheckInDto): Promise<CheckIn> {
    return await this._checkInService.updateById(id, updateCheckInDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<CheckIn>  {
    return await this._checkInService.deleteById(id);
  }
}
