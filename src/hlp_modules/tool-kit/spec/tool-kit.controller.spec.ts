import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitController } from '../tool-kit.controller';
import { ToolKitService } from '../tool-kit.service';

describe('ToolKitController', () => {
  let controller: ToolKitController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ToolKitController],
      providers: [ToolKitService],
    }).compile();

    controller = module.get<ToolKitController>(ToolKitController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
