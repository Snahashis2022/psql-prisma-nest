import { Test, TestingModule } from '@nestjs/testing';
import { ToolKitService } from '../tool-kit.service';

describe('ToolKitService', () => {
  let service: ToolKitService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ToolKitService],
    }).compile();

    service = module.get<ToolKitService>(ToolKitService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
