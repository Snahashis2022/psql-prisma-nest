import { IsUUID, IsString, IsNotEmpty } from "class-validator";

export class CreateToolKitDto {
    @IsString()
    @IsNotEmpty()
    title: string

    @IsString()
    @IsNotEmpty()
    description: string;
    // Relations
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    habit_id: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    membership_stage_id: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    membership_level_id: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    tool_kit_category_id: string;

    @IsUUID()
    @IsString()
    @IsNotEmpty()
    tool_kit_sub_category_id: string;
}
