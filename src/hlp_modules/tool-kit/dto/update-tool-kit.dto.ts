import { PartialType } from '@nestjs/mapped-types';
import { CreateToolKitDto } from './create-tool-kit.dto';

export class UpdateToolKitDto extends PartialType(CreateToolKitDto) {}
