import { PrismaClient, Tool_kit as ToolKit} from "@prisma/client";
import { CreateToolKitDto } from "./dto/create-tool-kit.dto";
import { UpdateToolKitDto } from "./dto/update-tool-kit.dto";


export class ToolKitRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<ToolKit[]>{
        try {
            const res: ToolKit[] = await this.client.tool_kit.findMany({
                include:{
                    habit: true,
                    membership_stage: true,
                    membership_level: true,
                    tool_kit_category: true,
                    tool_kit_sub_category: true,
                    schedules: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<ToolKit>{
        try {
            const res: ToolKit = await this.client.tool_kit.findUnique({
                where: {id: id},
                include:{
                    habit: true,
                    membership_stage: true,
                    membership_level: true,
                    tool_kit_category: true,
                    tool_kit_sub_category: true,
                    schedules: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<ToolKit>{
        try {
            const res: ToolKit = await this.client.tool_kit.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createToolKitDto: CreateToolKitDto): Promise<ToolKit>{
        try {
            const res = await this.client.tool_kit.create({
                data: createToolKitDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateToolKitDto: UpdateToolKitDto): Promise<ToolKit>{
        try {
            const res = await this.client.tool_kit.update({
                where: {id: id},
                data: updateToolKitDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}