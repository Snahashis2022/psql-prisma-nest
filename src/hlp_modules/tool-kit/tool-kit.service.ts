import { Injectable } from '@nestjs/common';
import { CreateToolKitDto } from './dto/create-tool-kit.dto';
import { UpdateToolKitDto } from './dto/update-tool-kit.dto';
import { ToolKitRepo } from './tool-kit.repo';
import { Tool_kit as ToolKit} from "@prisma/client";

@Injectable()
export class ToolKitService {
  constructor(private readonly _toolKitRepo: ToolKitRepo){}
  async findAll(): Promise<ToolKit[]> {
    try {
        const res: ToolKit[] = await this._toolKitRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<ToolKit>{
    try {
        const res: ToolKit = await this._toolKitRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<ToolKit>{
    try {
        const res = await this._toolKitRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateToolKitDto: UpdateToolKitDto): Promise<ToolKit>{
    try {
        const res = await this._toolKitRepo.updateById(id, updateToolKitDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createToolKitDto: CreateToolKitDto): Promise<ToolKit> {
    try {
        const res = await this._toolKitRepo.create(createToolKitDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
