import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ToolKitService } from './tool-kit.service';
import { PrismaClient, Tool_kit as ToolKit} from "@prisma/client";
import { CreateToolKitDto } from './dto/create-tool-kit.dto';
import { UpdateToolKitDto } from './dto/update-tool-kit.dto';

@Controller('tool-kit')
export class ToolKitController {
  constructor(private readonly _toolKitService: ToolKitService) {}

  @Post('/create')
  async create(@Body() createToolKitDto: CreateToolKitDto): Promise<ToolKit> {
    return await this._toolKitService.create(createToolKitDto);
  }

  @Get('/all')
  async findAll(): Promise<ToolKit[]> {
    return await this._toolKitService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<ToolKit> {
    return await this._toolKitService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateToolKitDto: UpdateToolKitDto): Promise<ToolKit> {
    return await this._toolKitService.updateById(id, updateToolKitDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<ToolKit> {
    return await this._toolKitService.deleteById(id);
  }
}
