import { Module } from '@nestjs/common';
import { ToolKitService } from './tool-kit.service';
import { ToolKitController } from './tool-kit.controller';
import { ToolKitRepo } from './tool-kit.repo';

@Module({
  controllers: [ToolKitController],
  providers: [ToolKitService, ToolKitRepo]
})
export class ToolKitModule {}
