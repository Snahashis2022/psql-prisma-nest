import { Module } from '@nestjs/common';
import { MembershipLevelService } from './membership-level.service';
import { MembershipLevelController } from './membership-level.controller';
import { MembershipLevelRepo } from './memebership-level.repo';

@Module({
  controllers: [MembershipLevelController],
  providers: [MembershipLevelService, MembershipLevelRepo]
})
export class MembershipLevelModule {}
