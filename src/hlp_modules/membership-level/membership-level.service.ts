import { Injectable } from '@nestjs/common';
import { CreateMembershipLevelDto } from './dto/create-membership-level.dto';
import { UpdateMembershipLevelDto } from './dto/update-membership-level.dto';
import { MembershipLevelRepo } from './memebership-level.repo';
import { Membership_level as MembershipLevel} from "@prisma/client";

@Injectable()
export class MembershipLevelService {
  constructor(private readonly _membershipLevelRepo: MembershipLevelRepo){}

  async findAll(): Promise<MembershipLevel[]> {
    try {
        const res: MembershipLevel[] = await this._membershipLevelRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<MembershipLevel>{
    try {
        const res: MembershipLevel = await this._membershipLevelRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<MembershipLevel>{
    try {
        const res = await this._membershipLevelRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateMembershipLevelDto: UpdateMembershipLevelDto): Promise<MembershipLevel>{
    try {
        const res = await this._membershipLevelRepo.updateById(id, updateMembershipLevelDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createMembershipLevelDto: CreateMembershipLevelDto): Promise<MembershipLevel> {
    try {
        const res = await this._membershipLevelRepo.create(createMembershipLevelDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

  
  
}
