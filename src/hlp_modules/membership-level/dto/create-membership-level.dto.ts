import { IsString, IsNotEmpty } from "class-validator";

export class CreateMembershipLevelDto {
    @IsString()
    @IsNotEmpty()
    title: string

    @IsString()
    @IsNotEmpty()
    description: string;
}
