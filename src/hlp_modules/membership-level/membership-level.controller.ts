import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MembershipLevelService } from './membership-level.service';
import { Membership_level as MembershipLevel} from "@prisma/client";
import { CreateMembershipLevelDto } from './dto/create-membership-level.dto';
import { UpdateMembershipLevelDto } from './dto/update-membership-level.dto';

@Controller('membership-level')
export class MembershipLevelController {
  constructor(private readonly _membershipLevelService: MembershipLevelService) {}

  @Post('/create')
  async create(@Body() createMembershipLevelDto: CreateMembershipLevelDto): Promise<MembershipLevel> {
    return await this._membershipLevelService.create(createMembershipLevelDto);
  }

  @Get('/all')
  async findAll(): Promise<MembershipLevel[]> {
    return await this._membershipLevelService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<MembershipLevel> {
    return await this._membershipLevelService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateMembershipLevelDto: UpdateMembershipLevelDto): Promise<MembershipLevel> {
    return await this._membershipLevelService.updateById(id, updateMembershipLevelDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<MembershipLevel>  {
    return await this._membershipLevelService.deleteById(id);
  }
}
