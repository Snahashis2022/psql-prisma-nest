import { Schedule, PrismaClient, Membership_level as MembershipLevel} from "@prisma/client";
import { CreateMembershipLevelDto } from "./dto/create-membership-level.dto";
import { UpdateMembershipLevelDto } from "./dto/update-membership-level.dto";






export class MembershipLevelRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<MembershipLevel[]>{
        try {
            const res: MembershipLevel[] = await this.client.membership_level.findMany({
                include:{
                    tool_kits: true,
                    membership_stages: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<MembershipLevel>{
        try {
            const res: MembershipLevel = await this.client.membership_level.findUnique({
                where: {id: id},
                include:{
                    tool_kits: true,
                    membership_stages: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<MembershipLevel>{
        try {
            const res: MembershipLevel = await this.client.membership_level.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createMembershipLevelDto: CreateMembershipLevelDto): Promise<MembershipLevel>{
        try {
            const res = await this.client.membership_level.create({
                data: createMembershipLevelDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateMembershipLevelDto: UpdateMembershipLevelDto): Promise<MembershipLevel>{
        try {
            const res = await this.client.membership_level.update({
                where: {id: id},
                data: updateMembershipLevelDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}