import { Test, TestingModule } from '@nestjs/testing';
import { MembershipLevelController } from '../membership-level.controller';
import { MembershipLevelService } from '../membership-level.service';

describe('MembershipLevelController', () => {
  let controller: MembershipLevelController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MembershipLevelController],
      providers: [MembershipLevelService],
    }).compile();

    controller = module.get<MembershipLevelController>(MembershipLevelController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
