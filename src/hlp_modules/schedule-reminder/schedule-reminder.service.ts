import { Injectable } from '@nestjs/common';
import { CreateScheduleReminderDto } from './dto/create-schedule-reminder.dto';
import { UpdateScheduleReminderDto } from './dto/update-schedule-reminder.dto';
import { PrismaClient, Schedule_reminder as ScheduleReminder } from "@prisma/client";
import { ScheduleReminderRepo } from './schedule-reminder.repo';

@Injectable()
export class ScheduleReminderService {
  constructor(private readonly _scheduleReminderRepo: ScheduleReminderRepo){}
  async findAll(): Promise<ScheduleReminder[]> {
    try {
        const res: ScheduleReminder[] = await this._scheduleReminderRepo.findAll();
        return res;
    }catch(err) {
        throw err; 
    }
  }
  async findById(id: string): Promise<ScheduleReminder>{
    try {
        const res: ScheduleReminder = await this._scheduleReminderRepo.findById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async deleteById(id: string): Promise<ScheduleReminder>{
    try {
        const res = await this._scheduleReminderRepo.deleteById(id);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async updateById(id: string, updateScheduleReminderDto: UpdateScheduleReminderDto): Promise<ScheduleReminder>{
    try {
        const res = await this._scheduleReminderRepo.updateById(id, updateScheduleReminderDto);
        return res; 
    }catch(err) {
        throw err; 
    }
  }
  async create(createScheduleReminderDto: CreateScheduleReminderDto): Promise<ScheduleReminder> {
    try {
        const res = await this._scheduleReminderRepo.create(createScheduleReminderDto);
        return res;
    }catch(err) {
        throw err; 
    }
  }

}
