import { PrismaClient, Schedule_reminder as ScheduleReminder } from "@prisma/client";
import { CreateScheduleReminderDto } from "./dto/create-schedule-reminder.dto";
import { UpdateScheduleReminderDto } from "./dto/update-schedule-reminder.dto";



export class ScheduleReminderRepo{
    client: PrismaClient;
    constructor(){
        this.client = new PrismaClient();
    }
    
    async findAll(): Promise<ScheduleReminder[]>{
        try {
            const res: ScheduleReminder[] = await this.client.schedule_reminder.findMany({
                include:{
                    schedule: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async findById(id: string): Promise<ScheduleReminder>{
        try {
            const res: ScheduleReminder = await this.client.schedule_reminder.findUnique({
                where: {id: id},
                include:{
                    schedule: true
                }
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async deleteById(id: string): Promise<ScheduleReminder>{
        try {
            const res: ScheduleReminder = await this.client.schedule_reminder.delete({
                where: {id: id}
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    async create(createScheduleReminderDto: CreateScheduleReminderDto): Promise<ScheduleReminder>{
        try {
            const res = await this.client.schedule_reminder.create({
                data: createScheduleReminderDto
            });
            return res;
        }catch(err) {
            throw err; 
        }
    }
    async updateById(id: string, updateScheduleReminderDto: UpdateScheduleReminderDto): Promise<ScheduleReminder>{
        try {
            const res = await this.client.schedule_reminder.update({
                where: {id: id},
                data: updateScheduleReminderDto
            });
            return res; 
        }catch(err) {
            throw err; 
        }
    }
    

}