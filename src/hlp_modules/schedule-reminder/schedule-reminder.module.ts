import { Module } from '@nestjs/common';
import { ScheduleReminderService } from './schedule-reminder.service';
import { ScheduleReminderController } from './schedule-reminder.controller';
import { ScheduleReminderRepo } from './schedule-reminder.repo';

@Module({
  controllers: [ScheduleReminderController],
  providers: [ScheduleReminderService, ScheduleReminderRepo]
})
export class ScheduleReminderModule {}
