import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ScheduleReminderService } from './schedule-reminder.service';
import { CreateScheduleReminderDto } from './dto/create-schedule-reminder.dto';
import { UpdateScheduleReminderDto } from './dto/update-schedule-reminder.dto';
import { PrismaClient, Schedule_reminder as ScheduleReminder } from "@prisma/client";

@Controller('schedule-reminder')
export class ScheduleReminderController {
  constructor(private readonly _scheduleReminderService: ScheduleReminderService) {}

  @Post('/create')
  async create(@Body() createScheduleReminderDto: CreateScheduleReminderDto): Promise<ScheduleReminder> {
    return await this._scheduleReminderService.create(createScheduleReminderDto);
  }

  @Get('/all')
  async findAll(): Promise<ScheduleReminder[]> {
    return await this._scheduleReminderService.findAll();
  }

  @Get('/one/:id')
  async findById(@Param('id') id: string): Promise<ScheduleReminder> {
    return await this._scheduleReminderService.findById(id);
  }

  @Patch('/update/:id')
  async updateById(@Param('id') id: string, @Body() updateScheduleReminderDto: UpdateScheduleReminderDto): Promise<ScheduleReminder> {
    return await this._scheduleReminderService.updateById(id, updateScheduleReminderDto);
  }

  @Delete('/delete/:id')
  async deleteById(@Param('id') id: string): Promise<ScheduleReminder>  {
    return await this._scheduleReminderService.deleteById(id);
  }
}
