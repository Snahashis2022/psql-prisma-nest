import { IsString, IsNotEmpty, IsUUID } from "class-validator";

export class CreateScheduleReminderDto {
    @IsString()
    @IsNotEmpty()
    reminder_time: string;
    //Relations
    @IsUUID()
    @IsString()
    @IsNotEmpty()
    schedule_id: string;
}
