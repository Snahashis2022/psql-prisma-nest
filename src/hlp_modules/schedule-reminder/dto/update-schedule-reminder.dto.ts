import { PartialType } from '@nestjs/mapped-types';
import { CreateScheduleReminderDto } from './create-schedule-reminder.dto';

export class UpdateScheduleReminderDto extends PartialType(CreateScheduleReminderDto) {}
