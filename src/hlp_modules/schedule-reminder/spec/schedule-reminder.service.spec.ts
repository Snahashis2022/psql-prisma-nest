import { Test, TestingModule } from '@nestjs/testing';
import { ScheduleReminderService } from '../schedule-reminder.service';

describe('ScheduleReminderService', () => {
  let service: ScheduleReminderService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ScheduleReminderService],
    }).compile();

    service = module.get<ScheduleReminderService>(ScheduleReminderService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
