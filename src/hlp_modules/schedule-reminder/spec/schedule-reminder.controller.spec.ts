import { Test, TestingModule } from '@nestjs/testing';
import { ScheduleReminderController } from '../schedule-reminder.controller';
import { ScheduleReminderService } from '../schedule-reminder.service';

describe('ScheduleReminderController', () => {
  let controller: ScheduleReminderController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ScheduleReminderController],
      providers: [ScheduleReminderService],
    }).compile();

    controller = module.get<ScheduleReminderController>(ScheduleReminderController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
