import { Module } from '@nestjs/common';
import { CheckInModule } from './check-in/check-in.module';
import { HabitController } from './habit/habit.controller';
import { HabitModule } from './habit/habit.module';
import { MembershipLevelModule } from './membership-level/membership-level.module';
import { MembershipStageModule } from './membership-stage/membership-stage.module';
import { ScheduleReminderModule } from './schedule-reminder/schedule-reminder.module';
import { ScheduleModule } from './schedule/schedule.module';
import { ToolKitCategoryModule } from './tool-kit-category/tool-kit-category.module';
import { ToolKitSubCategoryModule } from './tool-kit-sub-category/tool-kit-sub-category.module';
import { ToolKitModule } from './tool-kit/tool-kit.module';

@Module({
    imports: [
        ScheduleModule,
        ToolKitModule,
        CheckInModule,
        ToolKitCategoryModule,
        ToolKitSubCategoryModule,
        ScheduleReminderModule,
        MembershipStageModule,
        MembershipLevelModule,
        HabitModule
    ],
    controllers: [],
    providers: [
    ],
    exports: [
        ScheduleModule,
        ToolKitModule,
        CheckInModule,
        ToolKitCategoryModule,
        ToolKitSubCategoryModule,
        ScheduleReminderModule,
        MembershipStageModule,
        MembershipLevelModule,
        HabitModule
    ]
})
export class HlpModule { }
