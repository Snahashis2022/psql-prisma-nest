[PostgreSQL Medium]

SELECT name, age, (age * .10) AS calculated_field FROM person;
SELECT COALESCE(age, 0) FROM person;
copy(SELECT * FROM person) TO '/McSpraint/myapps/Office works/r.csv' DELIMITER ',' CSV HEADER;