CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE tag(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    description TEXT NOT NULL,
    tag VARCHAR(50) NOT NULL
);
CREATE TABLE person_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    personId UUID REFERENCES person(id) ,
    postId UUID REFERENCES post(id)
);

CREATE TABLE person(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    gender VARCHAR(7) NOT NULL,
    age INTEGER,
    UNIQUE(email)
);



CREATE TABLE tag_post(
    id UUID DEFAULT uuid_generate_v4() NOT NULL PRIMARY KEY,
    tagId UUID REFERENCES tag(id),
    postId UUID REFERENCES post(id)
);
SELECT * FROM tag_post 
    JOIN tag ON tag_post.tagId = tag.id
    JOIN post ON tag_post.postId = post.id;
INSERT INTO person_post(personId, postId)VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', '2b9eda92-7952-449f-a199-e292dfdbd2bc');
INSERT INTO person_post(personId, postId)VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', 'b7029a17-8023-434a-a199-488e96b1da5d');
INSERT INTO person_post(personId, postId)VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', 'bd649e73-ade9-4147-bf08-1b120d4efa80');
INSERT INTO person_post(personId, postId)VALUES('ccb0b06a-5126-4b65-aea4-0cf0c907e4ca', '559eda65-9e23-40b4-a97c-5273d52c4fa6');





INSERT INTO tag (title, description)VALUES('Jokes', 'This tag applies to jokes only');
INSERT INTO tag (title, description)VALUES('Story', 'This tag applies to stories only');
INSERT INTO tag (title, description)VALUES('Poem', 'This tag applies to poems only');

INSERT INTO post (title, description, tag)VALUES('1st title', 'This is my 1st post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('2nd title', 'This is my 2nd post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('3rd title', 'This is my 3rd post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('4th title', 'This is my 4th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('5th title', 'This is my 5th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('6th title', 'This is my 6th post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('My title1', 'This is just a post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('My title2', 'This is another post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('My title3', 'This is spamming post', 'Jokes');
INSERT INTO post (title, description, tag)VALUES('My title4', 'This is a test post', 'Jokes');

INSERT INTO person (name, email, password, gender, age)VALUES('Snahashis Das', 'snahashis@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Sayan Das', 'sayan@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Ayanava Paul', 'ayanava@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Avijit Sarkar', 'avijit@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Musabbar Hossain', 'musabbar@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Prithvi Ghosh', 'prithvi@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Ritam Chatterjee', 'ritam@gmail.com', 'test1234', 'Male', 22);
INSERT INTO person (name, email, password, gender, age)VALUES('Rohit Das', 'rohit@gmail.com', 'test1234', 'Male', 26);
INSERT INTO person (name, email, password, gender, age)VALUES('Rohit Das', 'rohit2@gmail.com', 'test1234', 'Male', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Saurav Daniel', 'saurav@gmail.com', 'test1234', 'Male', 27);
INSERT INTO person (name, email, password, gender, age)VALUES('Priyanka Das', 'priyanka954@gmail.com', 'test1234','Female', 20);
INSERT INTO person (name, email, password, gender, age)VALUES('Mou Roy', 'mou@gmail.com', 'test1234','Female', 21);
INSERT INTO person (name, email, password, gender, age)VALUES('Dipanwita Jana', 'dipanwita4@gmail.com', 'tet1234','Female', 24);
INSERT INTO person (name, email, password, gender, age)VALUES('Ipshita Sannal', 'ipshita@gmail.com', 'test1234','Female, 25');
INSERT INTO person (name, email, password, gender, age)VALUES('Ruhi Khatun', 'ruhi@gmail.com', 'test1234','Female', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Sushmita Sen', 'sushmia@gmail.com', 'test1234','Female', 22);
INSERT INTO person (name, email, password, gender, age)VALUES('Manashi Bagchi', 'manashi@gmail.com', 'test1234','Female', 23);
INSERT INTO person (name, email, password, gender, age)VALUES('Amrita Sarkar', 'amrita@gmail.com', 'test1234','Female', 25);
INSERT INTO person (name, email, password, gender, age)VALUES('Dipshikha Thakur', 'dipshikha@gmail.com', 'test1234','Female', 22);

INSERT INTO person_post(personId, postId)
    VALUES('149de7c7-b1f9-4a43-a687-daf8d3cf1274', '28532c63-e09e-4fff-b563-96bd5ab41926');
INSERT INTO person_post(personId, postId)
    VALUES('149de7c7-b1f9-4a43-a687-daf8d3cf1274', '30c95e00-0c4a-4f47-843d-0094bf8cd52c');
INSERT INTO person_post(personId, postId)
    VALUES('149de7c7-b1f9-4a43-a687-daf8d3cf1274', 'b50c85f3-742a-4b30-ac53-cbec02c8700f');
INSERT INTO person_post(personId, postId)
    VALUES('149de7c7-b1f9-4a43-a687-daf8d3cf1274', 'ec44075b-de86-4588-bcaf-035a12790055');

INSERT INTO person_post(personId, postId)
    VALUES('', '');

INSERT INTO tag_post(tagId, postId)
    VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', '28532c63-e09e-4fff-b563-96bd5ab41926');
INSERT INTO tag_post(tagId, postId)
    VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', '30c95e00-0c4a-4f47-843d-0094bf8cd52c');
INSERT INTO tag_post(tagId, postId)
    VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', 'b50c85f3-742a-4b30-ac53-cbec02c8700f');
INSERT INTO tag_post(tagId, postId)
    VALUES('aad4593a-d7f6-49f2-9370-316bdd4a3bd8', 'ec44075b-de86-4588-bcaf-035a12790055');


SELECT Count(*) FROM tag_post GROUP BY tagId;

SELECT personId, COUNT(*) from person_post GROUP BY personId;

SELECT * FROM person_post
    JOIN person ON person_post.personId = person.id
    JOIN post ON person_post.postId = post.id;

SELECT * FROM tag_post
    JOIN tag ON tag_post.tagId = tag.id
    JOIN post ON tag_post.postId = post.id;

SELECT personId, COUNT(*) FROM person_post
    JOIN person ON person_post.personId = person.id
    JOIN post ON person_post.postId = post.id
        GROUP BY personId;
        

SELECT * FROM person;
