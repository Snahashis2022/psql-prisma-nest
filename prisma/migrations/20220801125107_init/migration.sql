-- CreateEnum
CREATE TYPE "UserRole" AS ENUM ('STUDENT', 'TEACHER');

-- CreateEnum
CREATE TYPE "ScheduleFor" AS ENUM ('TOOL_KIT', 'CHECK_IN');

-- CreateEnum
CREATE TYPE "ScheduleType" AS ENUM ('ONE_TIME', 'DAILY', 'WEEKLY', 'MONTHLY');

-- CreateEnum
CREATE TYPE "AgeGroup" AS ENUM ('ADULT', 'CHILD', 'OLD');

-- CreateTable
CREATE TABLE "User" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "password" TEXT NOT NULL,
    "gender" TEXT NOT NULL,
    "age" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Course" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Course_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "CourseEnrollment" (
    "id" TEXT NOT NULL,
    "role" "UserRole" NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "userId" TEXT NOT NULL,
    "courseId" TEXT NOT NULL,

    CONSTRAINT "CourseEnrollment_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Test" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "courseId" TEXT NOT NULL,

    CONSTRAINT "Test_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TestResult" (
    "id" TEXT NOT NULL,
    "result" DECIMAL(65,30) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "testId" TEXT NOT NULL,

    CONSTRAINT "TestResult_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Schedule" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "schedule_for" "ScheduleFor" NOT NULL,
    "schedule_type" "ScheduleType" NOT NULL,
    "start_date" TIMESTAMP(3) NOT NULL,
    "show_reminder" BOOLEAN NOT NULL,
    "repeat_per_day" INTEGER NOT NULL,
    "repeat_per_month" INTEGER[],
    "schedule_days" TEXT[],
    "is_schedule_disabled" BOOLEAN NOT NULL,
    "check_in_id" TEXT NOT NULL,
    "tool_kit_id" TEXT NOT NULL,

    CONSTRAINT "Schedule_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Schedule_reminder" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "reminder_time" TIMESTAMP(3) NOT NULL,
    "schedule_id" TEXT NOT NULL,

    CONSTRAINT "Schedule_reminder_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Check_in" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "avatar" TEXT,
    "hlp_reward_points" INTEGER,

    CONSTRAINT "Check_in_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Tool_kit" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "short_description" TEXT,
    "video_thumb_nail" TEXT,
    "tool_kit_info" TEXT,
    "video_url" TEXT,
    "image_url" TEXT,
    "image_id" TEXT,
    "file_path" TEXT,
    "activity_timer_value" TEXT,
    "meditation_timer_value" TEXT,
    "podcast_audio_url" TEXT,
    "podcast_audio_length" TEXT,
    "sleep_check_max_value" DECIMAL(65,30),
    "max_foot_steps" DECIMAL(65,30),
    "max_heart_rate_in_bpm" DECIMAL(65,30),
    "max_blood_systolic_value" INTEGER,
    "max_blood_diastolic_value" INTEGER,
    "max_weight_value" INTEGER,
    "max_medication_per_day_value" INTEGER,
    "max_alcohol_intake" INTEGER,
    "max_ecg_spm_value" INTEGER,
    "todo_screen_description" TEXT,
    "is_whats_new_tool_kit" BOOLEAN,
    "time_spent_on_sports" INTEGER,
    "membership_stage_type" TEXT,
    "tool_kit_type" TEXT,
    "tool_kit_hlp_reward_points" INTEGER,
    "tool_kit_profile_page_image_url" TEXT,
    "tool_kit_profile_page_image_id" TEXT,
    "tool_kit_profile_page_file_path" TEXT,
    "tool_kit_explain_page_image_url" TEXT,
    "tool_kit_explain_page_image_id" TEXT,
    "tool_kit_explain_page_file_path" TEXT,
    "habit_id" TEXT NOT NULL,
    "membership_stage_id" TEXT NOT NULL,
    "membership_level_id" TEXT NOT NULL,
    "tool_kit_category_id" TEXT NOT NULL,
    "tool_kit_sub_category_id" TEXT NOT NULL,

    CONSTRAINT "Tool_kit_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Tool_kit_category" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "age_group" "AgeGroup" NOT NULL,
    "avatar" TEXT,
    "image_url" TEXT,
    "image_id" TEXT,
    "file_path" TEXT,

    CONSTRAINT "Tool_kit_category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Tool_kit_sub_category" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "tool_kit_category_id" TEXT NOT NULL,

    CONSTRAINT "Tool_kit_sub_category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Membership_level" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "hlp_reward_points_to_complete_this_level" TEXT,
    "hlp_reward_points" INTEGER,
    "sequence_number" INTEGER,

    CONSTRAINT "Membership_level_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Membership_stage" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "emoji" TEXT,
    "hlp_reward_points_to_unlock_this_stage" INTEGER,
    "number_of_donations" INTEGER,
    "account_duration" INTEGER,
    "hlp_reward_points" INTEGER,
    "sequence_number" INTEGER,
    "color_code" TEXT,
    "membership_level_id" TEXT NOT NULL,

    CONSTRAINT "Membership_stage_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Habit" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL,

    CONSTRAINT "Habit_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- AddForeignKey
ALTER TABLE "CourseEnrollment" ADD CONSTRAINT "CourseEnrollment_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "CourseEnrollment" ADD CONSTRAINT "CourseEnrollment_courseId_fkey" FOREIGN KEY ("courseId") REFERENCES "Course"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Test" ADD CONSTRAINT "Test_courseId_fkey" FOREIGN KEY ("courseId") REFERENCES "Course"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TestResult" ADD CONSTRAINT "TestResult_testId_fkey" FOREIGN KEY ("testId") REFERENCES "Test"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Schedule" ADD CONSTRAINT "Schedule_check_in_id_fkey" FOREIGN KEY ("check_in_id") REFERENCES "Check_in"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Schedule" ADD CONSTRAINT "Schedule_tool_kit_id_fkey" FOREIGN KEY ("tool_kit_id") REFERENCES "Tool_kit"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Schedule_reminder" ADD CONSTRAINT "Schedule_reminder_schedule_id_fkey" FOREIGN KEY ("schedule_id") REFERENCES "Schedule"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit" ADD CONSTRAINT "Tool_kit_habit_id_fkey" FOREIGN KEY ("habit_id") REFERENCES "Habit"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit" ADD CONSTRAINT "Tool_kit_membership_stage_id_fkey" FOREIGN KEY ("membership_stage_id") REFERENCES "Membership_stage"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit" ADD CONSTRAINT "Tool_kit_membership_level_id_fkey" FOREIGN KEY ("membership_level_id") REFERENCES "Membership_level"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit" ADD CONSTRAINT "Tool_kit_tool_kit_category_id_fkey" FOREIGN KEY ("tool_kit_category_id") REFERENCES "Tool_kit_category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit" ADD CONSTRAINT "Tool_kit_tool_kit_sub_category_id_fkey" FOREIGN KEY ("tool_kit_sub_category_id") REFERENCES "Tool_kit_sub_category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Tool_kit_sub_category" ADD CONSTRAINT "Tool_kit_sub_category_tool_kit_category_id_fkey" FOREIGN KEY ("tool_kit_category_id") REFERENCES "Tool_kit_category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Membership_stage" ADD CONSTRAINT "Membership_stage_membership_level_id_fkey" FOREIGN KEY ("membership_level_id") REFERENCES "Membership_level"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
